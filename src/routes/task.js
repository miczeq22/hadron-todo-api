import * as service from '../services/taskService';

const routesConfig = () => {
    return {
        insertTask: {
            path: '/api/task',
            methods: ['POST'],
            callback: service.insert,
        },
        updateTask: {
            path: '/api/task/:id',
            methods: ['PUT'],
            callback: service.update,
        },
        findOneTaskById: {
            path: '/api/task/:id',
            methods: ['GET'],
            callback: service.findOneById,
        },
        findAllTasksByUser: {
            path: '/api/task/byUser/:id',
            methods: ['GET'],
            callback: service.findAllByUserId,
        },
        removeTask: {
            path: '/api/task/:id',
            methods: ['DELETE'],
            callback: service.remove,
        },
    };
};

module.exports = routesConfig;