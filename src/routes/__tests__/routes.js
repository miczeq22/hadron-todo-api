import chai, { expect } from 'chai';
import expressApp, { container, stopServer } from '../../index';
import chaiHttp from 'chai-http';

chai.use(chaiHttp);

describe ('HTTP Test', () => {
    let server;
    let adminToken = '';
    let userToken = '';

    before((done) => {
        expressApp.on('appStarted', () => {
            server = expressApp.listen(done);
        });
    });

    before (async () => {
        await chai.request(expressApp).post('/api/role').send({ name: 'Admin' });
        await chai.request(expressApp).post('/api/register').send({ username: 'admin', password: 'admin123' });

        await chai.request(expressApp).post('/api/role').send({ name: 'User' });
        await chai.request(expressApp).post('/api/register').send({ username: 'user', password: 'user1234' });

        const roleRepository = container.take('roleRepository');
        const userRepository = container.take('userRepository');

        const admin = await userRepository.findOne({ where: { username: 'admin' }, relations: ['roles'] });
        const adminRole = await roleRepository.findOne({ name: 'Admin' });
        admin.roles.push(adminRole);
        await userRepository.save(admin);

        const user = await userRepository.findOne({ where: { username: 'user' }, relations: ['roles'] });
        const userRole = await roleRepository.findOne({ name: 'User' });
        user.roles.push(userRole);
        await userRepository.save(user);

        const adminResponse = await chai.request(expressApp).post('/api/login').send({ username: 'admin', password: 'admin123' });
        adminToken = adminResponse.body.token;

        const userResponse = await chai.request(expressApp).post('/api/login').send({ username: 'user', password: 'user1234' });
        userToken = userResponse.body.token;
    });

    describe('Role routes', () => {
        it ('GET /api/role should respond with 200 status and array in body', async () => {
            const response = await chai.request(expressApp).get('/api/role');
            expect(response.status).to.be.equal(200);
            expect(response.body.roles).to.be.an.instanceof(Array);
        });
    
        it ('GET /api/role/:id should respond with 404 status if role does not exists', async () => {
            const response = await chai.request(server).get('/api/role/101');
            expect(response.status).to.be.equal(404);
            expect(response.body.error).to.be.an('object');
        });
    
        it ('POST /api/role should respond with 201 status if request body is valid', async () => {
            const response = await chai.request(server).post('/api/role').send({ name: 'Guest' });
            expect(response.status).to.be.equal(201);
            expect(response.body.message).to.be.an('string');
        });
    
        it ('POST /api/role should respond with 400 status and error due to existing role', async () => {
            const response = await chai.request(server).post('/api/role').send({ name: 'Guest' });
            expect(response.status).to.be.equal(400);
            expect(response.body.error).to.be.an('object');
        });
    
        it ('GET /api/role/:id should response with status 200 and role object', async () => {
            const response = await chai.request(server).get('/api/role/3');
            expect(response.status).to.be.equal(200);
            expect(response.body.role).to.be.an('object');
        });
    
        it ('POST /api/role should respond with 400 status and error due to invalid request body', async () => {
            const response = await chai.request(server).post('/api/role').send({});
            expect(response.status).to.be.equal(400);
            expect(response.body.error).to.be.an('object');
        });
    
        it ('GET /api/role/byName/:name should respond with status 200 and role object', async () => {
            const response = await chai.request(server).get('/api/role/byName/Guest');
            expect(response.status).to.be.equal(200);
            expect(response.body.role).to.be.an('object');
        });
    
        it ('GET /api/role/byName/:name should respond with status 404 and error if role does not exists.', async () => {
            const response = await chai.request(server).get('/api/role/byName/DoesNotExists');
            expect(response.status).to.be.equal(404);
            expect(response.body.error).to.be.an('object');
        });
    
        it ('PUT /api/role/:id should respond with 201 status', async () => {
            const response = await chai.request(server).put('/api/role/3').send({ name: 'guest' });
            expect(response.status).to.be.equal(201);
            expect(response.body.message).to.be.an('string');
        });
    
        it ('PUT /api/role/:id should respond with 500 status if body is invalid', async () => {
            const response = await chai.request(server).put('/api/role/1').send({});
            expect(response.status).to.be.equal(500);
            expect(response.body.error).to.be.an('object');
        });
    
        it ('PUT /api/role/:id should respond with 404 status if role does not exists', async () => {
            const response = await chai.request(server).put('/api/role/101').send({ name: 'Guest' });
            expect(response.status).to.be.equal(404);
            expect(response.body.error).to.be.an('object');
        });
    
        it ('DELETE /api/role/:id should respond with 404 status if role does not exists', async () => {
            const response = await chai.request(server).delete('/api/role/101');
            expect(response.status).to.be.equal(404);
            expect(response.body.error).to.be.an('object');
        });
    
        it ('DELETE /api/role/:id should respond with 201 status', async () => {
            const response = await chai.request(server).delete('/api/role/3');
            expect(response.status).to.be.equal(201);
            expect(response.body.message).to.be.an('string');
        });
    });

    describe('User Routes', () => {
        it ('POST /api/register should respond with 201 status', async () => {
            const response = await chai.request(server).post('/api/register').send({
                username: 'guest',
                password: 'guest123',
            });
    
            expect(response.status).to.be.equal(201);
            expect(response.body.message).to.be.an('string');
        });
    
        it ('POST /api/register should respond with 400 status if user with provided username already exists', async () => {
            const response = await chai.request(server).post('/api/register').send({
                username: 'guest',
                password: 'guest123',
            });
    
            expect(response.status).to.be.equal(400);
            expect(response.body.error).to.be.an('object');
        });
    
        it ('POST /api/register should respond with 400 status if provided data is invalid', async () => {
            const response = await chai.request(server).post('/api/register').send({
                username: 'guest',
                password: 'guest',
            });
    
            expect(response.status).to.be.equal(400);
            expect(response.body.error).to.be.an('object');
        });
    
        it ('PUT /api/user/:id should respond with 400 status if provided data is invalid', async () => {
            const response = await chai.request(server).put('/api/user/1').send({
                oldPassword: 'guest123',
            });
    
            expect(response.status).to.be.equal(400);
            expect(response.body.error).to.be.an('object');
        });
    
        it ('PUT /api/user/:id should respond with 404 status if user does not exists', async () => {
            const response = await chai.request(server).put('/api/user/101').send({
                oldPassword: 'guest123',
                newPassword: 'Guest123'
            });
    
            expect(response.status).to.be.equal(404);
            expect(response.body.error).to.be.an('object');
        });
    
        it ('PUT /api/user/:id should respond with 201 status', async () => {
            const response = await chai.request(server).put('/api/user/3').send({
                oldPassword: 'guest123',
                newPassword: 'Guest123'
            });
    
            expect(response.status).to.be.equal(201);
            expect(response.body.message).to.be.an('string');
        });
    
        it ('GET /api/user/byUsername/:username should respond with 404 status if user does not exists', async () => {
            const response = await chai.request(server).get('/api/user/byUsername/qwe');
    
            expect(response.status).to.be.equal(404);
            expect(response.body.error).to.be.an('object');
        });
    
        it ('GET /api/user/byUsername/:username should respond with 200 status and user object', async () => {
            const response = await chai.request(server).get('/api/user/byUsername/guest');
    
            expect(response.status).to.be.equal(200);
            expect(response.body.user).to.be.an('object');
        });
    
        it ('GET /api/user/:id should respond with 404 status if user does not exists', async () => {
            const response = await chai.request(server).get('/api/user/101');
            expect(response.status).to.be.equal(404);
            expect(response.body.error).to.be.an('object');
        });
    
        it ('GET /api/user/:id should respond with 200 status and user object', async () => {
            const response = await chai.request(server).get('/api/user/3');
            expect(response.status).to.be.equal(200);
            expect(response.body.user).to.be.an('object');
        });
    
        it ('GET /api/user should respond with 200 status and user list', async () => {
            const response = await chai.request(server).get('/api/user');
            expect(response.status).to.be.equal(200);
            expect(response.body.users).to.be.an('array');
        });
    
        it ('DELETE /api/user/:id should respond with 404 status if user does not exists', async () => {
            const response = await chai.request(server).delete('/api/user/101');
            expect(response.status).to.be.equal(404);
            expect(response.body.error).to.be.an('object');
        });
    
        it ('DELETE /api/user/:id should respond with 201 status', async () => {
            const response = await chai.request(server).delete('/api/user/3');
            expect(response.status).to.be.equal(201);
            expect(response.body.message).to.be.a('string');
        });
    });


    describe ('Task Routes', () => {
        it ('POST /api/task should respond with 403 status if token is not provided', async () => {
            const response = await chai.request(server).post('/api/task');
            expect(response.status).to.be.equal(403);
            expect(response.body.error).to.be.an('object');
        });

        it ('POST /api/task should respond with 403 status if user is not authorized', async () => {
            const response = await chai.request(server)
                .post('/api/task')
                .set('Authorization', userToken)
            expect(response.status).to.be.equal(403);
            expect(response.body.error).to.be.an('object');
        });

        it ('POST /api/task should respond with 403 status if user unathorized', async () => {
            const response = await chai.request(server).post('/api/task');
            expect(response.status).to.be.equal(403);
            expect(response.body.error).to.be.an('object');
        });

        it ('POST /api/task should respond with 500 status if body is invalid', async () => {
            const response = await chai.request(server).post('/api/task').set('Authorization', adminToken);
            expect(response.status).to.be.equal(500);
            expect(response.body.error).to.be.an('object');
        });

        it ('POST /api/task should respond with 404 status if user does not exists.', async () => {
            const response = await chai.request(server)
                .post('/api/task')
                .set('Authorization', adminToken)
                .send({
                    title: 'Some Title',
                    description: 'Some Description',
                    userId: 103,
                });

            expect(response.status).to.be.equal(404);
            expect(response.body.error).to.be.an('object');
        });

        it ('POST /api/task should respond with 201 status', async () => {
            const response = await chai.request(server)
                .post('/api/task')
                .set('Authorization', adminToken)
                .send({
                    title: 'Some Title',
                    description: 'Some Description',
                    userId: 2,
                });

            expect(response.status).to.be.equal(201);
            expect(response.body.message).to.be.a('string');
        });

        it ('PUT /api/task/:id should respond with 403 if user is not authorized', async () => {
            const response = await chai.request(server)
                .put('/api/task/101')
                .set('Authorization', userToken)
    
            expect(response.status).to.be.equal(403);
            expect(response.body.error).to.be.an('object');
        });

        it ('PUT /api/task/:id should respond with 404 status if task does not exists.', async () => {
            const response = await chai.request(server)
                .put('/api/task/101')
                .set('Authorization', adminToken)
                .send({
                    title: 'Some Title',
                    description: 'Some Description',
                    userId: 2,
                });
    
            expect(response.status).to.be.equal(404);
            expect(response.body.error).to.be.an('object');
        });

        it ('PUT /api/task/:id should respond with 500 status if body is invalid.', async () => {
            const response = await chai.request(server)
                .put('/api/task/1')
                .set('Authorization', adminToken)
                .send({
                    description: 'Some Description',
                    userId: 2,
                });
    
            expect(response.status).to.be.equal(500);
            expect(response.body.error).to.be.an('object');
        });

        it ('PUT /api/task/:id should respond with 201 status.', async () => {
            const response = await chai.request(server)
                .put('/api/task/1')
                .set('Authorization', adminToken)
                .send({
                    title: 'New Title',
                    description: 'New Description',
                    userId: 2,
                });
    
            expect(response.status).to.be.equal(201);
            expect(response.body.message).to.be.a('string');
        });

        it ('GET /api/task/:id should respond with 403 status if non authorized', async () => {
            const response = await chai.request(server).get('/api/task/1');

            expect(response.status).to.be.equal(403);
            expect(response.body.error).to.be.an('object');
        });

        it ('GET /api/task/:id should respond with 404 status if task does not exists', async () => {
            const response = await chai.request(server)
                .get('/api/task/101')
                .set('Authorization', userToken);

            expect(response.status).to.be.equal(404);
            expect(response.body.error).to.be.an('object');
        });

        it ('GET /api/task/:id should respond with 200 status and task object', async () => {
            const response = await chai.request(server)
                .get('/api/task/1')
                .set('Authorization', userToken);

            expect(response.status).to.be.equal(200);
            expect(response.body.task).to.be.an('object');
        });

        it ('GET /api/task/byUser/:id should respond with 403 status if non authorized', async () => {
            const response = await chai.request(server)
                .get('/api/task/byUser/1');

            expect(response.status).to.be.equal(403);
            expect(response.body.error).to.be.an('object');
        });

        it ('GET /api/task/byUser/:id should respond with 404 status if user does not exists', async () => {
            const response = await chai.request(server)
                .get('/api/task/byUser/101')
                .set('Authorization', userToken);

            expect(response.status).to.be.equal(404);
            expect(response.body.error).to.be.an('object');
        });

        it ('GET /api/task/byUser/:id should respond with 200 status and task array', async () => {
            const response = await chai.request(server)
                .get('/api/task/byUser/1')
                .set('Authorization', userToken);

            expect(response.status).to.be.equal(200);
            expect(response.body.tasks).to.be.instanceOf(Array);
        });

        it ('DELETE /api/task/:id should respond with 403 status if user not authorized', async () => {
            const response = await chai.request(server)
                .delete('/api/task/1')
                .set('Authorization', userToken);

                expect(response.status).to.be.equal(403);
                expect(response.body.error).to.be.an('object');
        });

        it ('DELETE /api/task/:id should respond with 404 status if task does not exists.', async () => {
            const response = await chai.request(server)
                .delete('/api/task/101')
                .set('Authorization', adminToken);

                expect(response.status).to.be.equal(404);
                expect(response.body.error).to.be.an('object');
        });

        it ('DELETE /api/task/:id should respond with 201 status', async () => {
            const response = await chai.request(server)
                .delete('/api/task/1')
                .set('Authorization', adminToken);

                expect(response.status).to.be.equal(201);
                expect(response.body.message).to.be.a('string');
        });
    });

    after(async () => {
        const connection = container.take('connection');
        await connection.dropDatabase();
        server.close();
    });
});
