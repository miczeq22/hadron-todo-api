import * as service from '../services/userService';

const routesConfig = () => {
    return {
        insertUser: {
            path: '/api/register',
            methods: ['POST'],
            callback: service.insert,
        },
        updateUser: {
            path: '/api/user/:id',
            methods: ['PUT'],
            callback: service.update,
        },
        findOneUserByUsername: {
            path: '/api/user/byUsername/:username',
            methods: ['GET'],
            callback: service.findOneByUsername,
        },
        findOneUserById: {
            path: '/api/user/:id',
            methods: ['GET'],
            callback: service.findOneById,
        },
        findAllUsers: {
            path: '/api/user',
            methods: ['GET'],
            callback: service.findAll,
        },
        removeUser: {
            path: '/api/user/:id',
            methods: ['DELETE'],
            callback: service.remove,
        },
    };
};

module.exports = routesConfig;
