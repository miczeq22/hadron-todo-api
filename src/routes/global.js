import { login } from '../services/userService';

const routesConfig = () => {
    return {
        helloWorld: {
            path: '/',
            methods: ['GET'],
            callback: () => 'Hello World!',
        },
        version: {
            path: '/version',
            methods: ['GET'],
            callback: () => `App version: ${process.env.VERSION || '0.0.1'}`,
        },
        login: {
            path: '/api/login',
            methods: ['POST'],
            callback: login,
        },
    };
};

module.exports = routesConfig;
