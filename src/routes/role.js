import * as service from '../services/roleService';

const routesConfig = () => {
    return {
        insertRole: {
            path: '/api/role',
            methods: ['POST'],
            callback: service.insert,
        },
        updateRole: {
            path: '/api/role/:id',
            methods: ['PUT'],
            callback: service.update,
        },
        findOneRoleByName: {
            path: '/api/role/byName/:name',
            methods: ['GET'],
            callback: service.findOneByName,
        },
        findOneRoleById: {
            path: '/api/role/:id',
            methods: ['GET'],
            callback: service.findOneById,
        },
        findAllRoles: {
            path: '/api/role',
            methods: ['GET'],
            callback: service.findAll,
        },
        removeRole: {
            path: '/api/role/:id',
            methods: ['DELETE'],
            callback: service.remove,
        },
    };
};

module.exports = routesConfig;
