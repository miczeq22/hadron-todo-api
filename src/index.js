import hadron from '@brainhubeu/hadron-core';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import db from './config/db';
import testDb from './config/testDb';
import jsonProvider from '@brainhubeu/hadron-json-provider';
import securityConfig from './security/securityConfig';
import * as hadronExpress from '@brainhubeu/hadron-express';
import { expressMiddlewareProvider } from '@brainhubeu/hadron-security';
import UserProvider from './security/UserProvider';
import RoleProvider from './security/RoleProvider';
import dotenv from 'dotenv';
import http from 'http';

dotenv.config();

const port = process.env.PORT || 4000;
const expressApp = express();
const environment = process.env.ENVIRONMENT || 'TEST';
const database = environment === 'DEV' ? db : testDb;

expressApp.use(cors());
expressApp.use(bodyParser.json());

const hadronInit = async () => {
    return new Promise(async (resolve, reject) => {
        const config = {
            ...database,
            routes: await jsonProvider([`${__dirname}/routes/*`], ['js']),
        };
    
        const container = await hadron(expressApp, [
                require('@brainhubeu/hadron-typeorm'),
            ],
            config);
    
        const userProvider = new UserProvider(container.take('userRepository'));
        const roleProvider = new RoleProvider(container.take('roleRepository'));
    
        const security = await securityConfig(userProvider, roleProvider);
    
        expressApp.use(expressMiddlewareProvider(security));
    
        hadronExpress.register(container, config);
        
        expressApp.use((req, res, next) => {
            res.status(404).json({
                error: {
                    message: 'Not found.',
                },
            });
        });

        resolve(container);
    });
};

let container;
const server = http.createServer(expressApp);

hadronInit().then(containerInstance => {
    container = containerInstance;
    server.listen(port, () => {
        expressApp.emit('appStarted');
        console.log(`Listening on http://localhost:${port}`);
    });
});

export default expressApp;
export { container };
