import userSchema from '../schemas/User';
import roleSchema from '../schemas/Role';
import taskSchema from '../schemas/Task';

const connection = {
  name: 'mysql-connection',
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'my-secret-pw',
  database: 'todo-app-test',
  entitySchemas: [roleSchema, userSchema, taskSchema],
  synchronize: true,
};

export default {
    connection,
    entities: [roleSchema, userSchema, taskSchema]
};
