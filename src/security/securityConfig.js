import HadronSecurity from '@brainhubeu/hadron-security';

const securityConfig = (userProvider, roleProvider) => {
    return new Promise((resolve, reject) => {
        const security = new HadronSecurity(userProvider, roleProvider);

        security
            .allow('/api/task/**', ['Admin', 'NotExists'], ['post', 'put', 'delete', 'get'])
            .allow('/api/task/**', 'User', ['get']);

        resolve(security);
    });
};

export default securityConfig;
