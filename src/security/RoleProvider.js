class RoleProvider {
    constructor (roleRepository) {
        this.roleRepository = roleRepository;
    }

    async getRole(name) {
        return await this.roleRepository.findOne({ name });
    };

    async getRoles() {
        const roles = await this.roleRepository.find();
        return roles.map(role => role.name);
    };
}

export default RoleProvider;
