class UserProvider {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }

    async loadUserByUsername(username) {
        const user = await this.userRepository.findOne({where: { username }, relations: ['roles']});
        return user;
    };

    refreshUser(user) {};
}

export default UserProvider;
