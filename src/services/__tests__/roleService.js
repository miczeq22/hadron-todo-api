import { expect, assert } from 'chai';
import * as sinon from 'sinon';
import * as service from '../roleService';

describe ('Role Service', () => {
    let fakeRepository;

    beforeEach(() => {
        fakeRepository = {
            find: (arg) => fakeRepository,
            findOne: (arg) => fakeRepository,
            save: (args) => Promise.resolve(),
        };
    });
    
    it ('findOneById should execute findOne({ id }) on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves(true);
        fakeRepository.findOne = findOne;

        const req = {
            params: {
                id: 1,
            },
        };

        await service.findOneById(req, { roleRepository: fakeRepository });

        assert(findOne.calledWith({ id: 1 }));
    });    

    it ('findOneByName should execute findOne({ name }) on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves(true);
        fakeRepository.findOne = findOne;

        const req = {
            params: {
                name: 'Admin',
            },
        };

        await service.findOneByName(req, { roleRepository: fakeRepository });

        assert(findOne.calledWith({ name: 'Admin' }));
    });

    it ('findAll should execute find() on repository object', async () => {
        const find = sinon.stub();
        find.resolves(true);
        fakeRepository.find = find;

        await service.findAll({}, { roleRepository: fakeRepository });

        expect(find.calledOnce).to.be.equal(true);
    });

    it ('insert should execute save function with req body', async () => {
        const save = sinon.spy();
        fakeRepository.save = save;
        const req = {
            body: {
                name: 'Admin',
            },
        };

        await service.insert(req, { roleRepository: fakeRepository });
        const calledWith = save.firstCall.lastArg;

        expect(Object.keys(calledWith)).to.deep.equal([
            'name'
        ]);
    });

    it ('insert should fail due to missing name in body', async () => {
        const save = sinon.spy();
        fakeRepository.save = save;

        const req = {
            body: {},
        };

        await service.insert(req, { roleRepository: fakeRepository });

        expect(save.called).to.be.equal(false);
    });

    it ('update should execute findOne({ id }) and save on repository object', async () => {
        const save = sinon.spy();
        fakeRepository.save = save;

        const findOne = sinon.stub();
        fakeRepository.findOne = findOne;
        findOne.withArgs({ id: 1 }).resolves({ name: 'Admin' });
    
        const req = {
            params: { id: 1 },
            body: {
                name: 'Admin',
            },
        };

        await service.update(req, { roleRepository: fakeRepository });

        assert(findOne.calledWith({ id: 1 }));
        expect(save.calledOnce).to.be.equal(true);
    });

    it ('update should fail due to missing name in body', async () => {
        const save = sinon.spy();
        fakeRepository.save = save;

        const findOne = sinon.stub();
        fakeRepository.findOne = findOne;
        findOne.resolves(true);
    
        const req = {
            params: { id: 1 },
            body: { },
        };

        await service.update(req, { roleRepository: fakeRepository });

        expect(save.calledOnce).to.be.equal(false);
    });

    it ('remove should execute findOne({ id }) and removeById on repository object', async () => {
        const removeById = sinon.spy();
        fakeRepository.removeById = removeById;
        const findOne = sinon.stub();
        findOne.resolves(true);
        fakeRepository.findOne = findOne;

        const req = {
            params: { id: 1 },
        };

        await service.remove(req, { roleRepository:fakeRepository });

        assert(findOne.calledWith({ id: 1 }));
        expect(removeById.calledOnce).to.be.equal(true);
    });

    it ('remove should fail due to not founded id ', async () => {
        const removeById = sinon.spy();
        fakeRepository.removeById = removeById;
        const findOne = sinon.stub();
        findOne.resolves(false);
        fakeRepository.findOne = findOne;

        const req = {
            params: { id: 1 },
        };

        await service.remove(req, { roleRepository:fakeRepository });

        assert(findOne.calledWith({ id: 1 }));
        expect(removeById.calledOnce).to.be.equal(false);
    });
});
