import * as sinon from 'sinon';
import { expect, assert } from 'chai';
import * as service from '../userService';

describe ('User Service', () => {
    let fakeRepository;

    beforeEach(() => {
        fakeRepository = {
            find: (arg) => fakeRepository,
            findOne: (arg) => fakeRepository,
            save: (arg) => Promise.resolve(),
        };
    });

    it ('findOneByUsername should execute findOne({ username }) on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves(true);
        fakeRepository.findOne = findOne;

        const req = {
            params: {
                username: 'Admin',
            },
        };

        await service.findOneByUsername(req, { userRepository: fakeRepository });

        assert(findOne.calledWith({ where: { username: 'Admin' }, relations: ['roles'] }));
    });

    it ('findOneById should execute findOne({ id }) on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves(true);
        fakeRepository.findOne = findOne;

        const req = {
            params: {
                id: 1,
            },
        };

        await service.findOneById(req, { userRepository: fakeRepository });

        assert(findOne.calledWith({ where: { id: 1 }, relations: ['roles'] }));
    });

    it ('findAll should execute find() on repository object', async () => {
        const find = sinon.stub();
        find.resolves(true);
        fakeRepository.find = find;

        await service.findAll({}, { userRepository: fakeRepository });

        expect(find.calledOnce).to.be.equal(true);
    });

    it ('insert should execute findOne({ username }) and save on repository object', async () => {
        const findOne = sinon.stub();
        findOne.withArgs({ username: '', password: '' }).resolves(true);
        fakeRepository.findOne = findOne;
        const save = sinon.spy();
        fakeRepository.save = save;

        const req = {
            body: {
                username: 'admin',
                password: 'admin123'
            },
        };

        await service.insert(req, { userRepository: fakeRepository });

        assert(findOne.calledWith({ username: 'admin' }));
        expect(save.calledOnce).to.be.equal(true);
        const calledWith = save.firstCall.lastArg;

        expect(Object.keys(calledWith)).to.deep.equal([
            'username',
            'password',
            'passwordHash'
        ]);
    });

    it ('insert should fail due to missing "username" in body', async () => {
        const save = sinon.spy();

        const req = {
            body: {
                password: 'admin123',
            },
        };

        await service.insert(req, { userRepository: fakeRepository });

        expect(save.called).to.be.equal(false);
    });

    it ('update should execute findOne({ id }) and save on repository object', async () => {
        const findOne = sinon.stub();
        fakeRepository.findOne = findOne;
        findOne.withArgs({ id: 1 }).resolves({ passwordHash: '$2b$10$eiA0vzEIDxVoK8Oj0k3FFuLRL/rFJTrmRRHsukynMtmlHASaDGjBe' });

        const save = sinon.spy();
        fakeRepository.save = save;

        const req = {
            params: { id: 1 },
            body: {
                oldPassword: 'admin123',
                newPassword: 'admin123',
            },
        };

        await service.update(req, { userRepository: fakeRepository });
        assert(findOne.calledWith({ id: 1 }));
        expect(save.calledOnce).to.be.equal(true);
    });

    it ('update should fail due to missing "oldPassword" in body', async () => {
        const req = {
            params: { id: 1 },
            body: {
                newPassword: 'admin123'
            },
        };

        const save = sinon.spy();
        fakeRepository.save = save;

        await service.update(req, { userRepository: fakeRepository });

        expect(save.called).to.be.equal(false);
    });

    it ('update should if "oldPassword" is incorrect', async () => {
        const findOne = sinon.stub();
        findOne.withArgs({ id: 1 }).resolves({ passwordHash: 'qwe' });
        fakeRepository.findOne = findOne;

        const save = sinon.spy();
        fakeRepository.save = save;

        const req = {
            params: { id: 1 },
            body: {
                oldPassword: 'admin123',
                newPassword: 'admin123'
            },
        };

        await service.update(req, { userRepository: fakeRepository });

        expect(save.called).to.be.equal(false);
    });

    it ('remove should execute findOne({ id }) and removeById on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves(true);
        fakeRepository.findOne = findOne;

        const removeById = sinon.spy();
        fakeRepository.removeById = removeById;

        const req = {
            params: { id: 1 }
        };

        await service.remove(req, { userRepository: fakeRepository });

        assert(findOne.calledWith({ id: 1 }));
        expect(removeById.calledOnce).to.be.equal(true);
    });

    it ('login should execute findOne({ username }) on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves(true);
        fakeRepository.findOne = findOne;

        const req = {
            body: {
                username: 'admin',
                password: 'admin',
            },
        };

        await service.login(req, { userRepository: fakeRepository });

        assert(findOne.calledWith({ where: {username: 'admin' }, relations: ['roles'] }));
    });
});
