import { expect, assert } from 'chai';
import * as sinon from 'sinon';
import * as service from '../taskService';

describe ('Task Service', () => {
    let fakeRepository;

    beforeEach(() => {
        fakeRepository = {
            findOne: (args) => fakeRepository,
            find: (args) => fakeRepository,
            save: (args) => Promise.resolve(),
        };
    });

    it ('findOneById should execute findOne({ id }) on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves(true);
        fakeRepository.findOne = findOne;

        const req = {
            params: { id: 1 },
        };

        await service.findOneById(req, { taskRepository: fakeRepository });

        assert(findOne.calledWith({ id: 1 }));
    });

    it ('findAllByUserId should execute find({ userId }) and findOne({ id }) on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves({ id: 1});
        fakeRepository.findOne = findOne;
        const find = sinon.stub();
        find.resolves(true);
        fakeRepository.find = find;

        const req = {
            params: { id: 1 },
        };

        await service.findAllByUserId(req, { taskRepository: fakeRepository, userRepository: fakeRepository });

        assert(find.calledWith({ userId: 1 }));
        assert(findOne.calledWith({ id: 1 }));
    });

    it ('insert should execute save with req body on repository object', async () => {
        const save = sinon.spy();
        fakeRepository.save = save;

        const req = {
            body: {
                title: 'Some title',
                description: 'Some description',
                userId: 1,
            },
        };

        await service.insert(req, { taskRepository: fakeRepository, userRepository: fakeRepository });
        console.log(save.called);

        const calledWith = save.firstCall.lastArg;

        expect(Object.keys(calledWith)).to.deep.equal([
            'title',
            'description',
            'userId'
        ]);
    });

    it ('insert should fail due to missing title in req body', async () => {
        const save = sinon.spy();
        fakeRepository.save = save;

        const req = {
            body: {
                description: 'Some description',
                userId: 1,
            },
        };

        await service.insert(req, { taskRepository: fakeRepository });

        expect(save.calledOnce).to.be.equal(false);
    });

    it ('update should execute findOne({ id }) and save on repository object', async () => {
        const findOne = sinon.stub();
        findOne.withArgs({ id: 1 }).resolves({ title: 'Old task', description: 'Old desc', userId: 1 });
        fakeRepository.findOne = findOne;
        const userFindOne = sinon.stub();
        userFindOne.resolves(true);
        const userRepository = {
            findOne: userFindOne,
        };

        const save = sinon.spy();
        fakeRepository.save = save;

        const req = {
            params: { id: 1 },
            body: {
                title: 'New task',
                description: 'New description',
                userId: 2,
            },
        };

        await service.update(req, { userRepository, taskRepository: fakeRepository });

        const calledWith = save.firstCall.lastArg;

        assert(findOne.calledWith({ id: 1 }));
        expect(Object.keys(calledWith)).to.deep.equal([
            'title',
            'description',
            'userId'
        ]);
    });

    it ('update should fail due to missing title in req body', async () => {
        const save = sinon.spy();
        fakeRepository.save = save;

        const req = {
            params: { id: 1 },
            body: {
                description: 'New description',
                userId: 2,
            },
        };

        await service.update(req, { taskRepository: fakeRepository });

        expect(save.called).to.be.equal(false);
    });

    it ('remove should execute findOne({ id }) and removeById on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves(true);
        fakeRepository.findOne = findOne;

        const removeById = sinon.spy();
        fakeRepository.removeById = removeById;

        const req = {
            params: { id: 1 },
        };

        await service.remove(req, { taskRepository: fakeRepository });

        assert(findOne.calledWith({ id: 1 }));
        expect(removeById.calledOnce).to.be.equal(true);
    });

    it ('remove should fail due to not existing task', async () => {
        const findOne = sinon.stub();
        findOne.resolves(false);
        fakeRepository.findOne = findOne;

        const removeById = sinon.spy();
        fakeRepository.removeById = removeById;

        await service.remove({}, { taskRepository: fakeRepository });

        expect(removeById.called).to.be.equal(false);
    });

    it ('changeStatus should execute findOne({ id }) on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves(true);

        fakeRepository.findOne = findOne;

        const req = {
            params: { id: 1 },
        };

        await service.changeStatus(req, { taskRepository: fakeRepository });

        assert(findOne.calledWith({ id: 1 }));
    });
});
