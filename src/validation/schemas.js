import roleSchema from './role.json';
import insertUser from './insertUser.json';
import updateUser from './updateUser.json';
import insertTask from './insertTask.json';

const schemas = {
    role: roleSchema,
    insertUser,
    updateUser,
    insertTask,
};

export default schemas;
