import validatorFactory from '@brainhubeu/hadron-validation';
import schemas from './schemas';

export default validatorFactory(schemas);
