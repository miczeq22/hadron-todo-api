import Role from '../models/Role';

const roleSchema = {
    name: 'Role',
    target: Role,
    columns: {
        id: {
            primary: true,
            type: 'int',
            generated: true,
        },
        name: {
            type: 'varchar',
            unique: true,
        },
        addedOn: {
            type: 'timestamp',
        },
    },
};

export default roleSchema;
