import Task from '../models/Task';

const taskSchema = {
    name: 'Task',
    target: Task,
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true,
        },
        title: {
            type: "varchar",
        },
        description: {
            type: "varchar",
        },
        userId: {
            type: 'int',
            required: true,
        },
        completed: {
            type: 'tinyint',
            default: false,
        },
        addedOn: {
            type: 'timestamp',
        },
    },
};

export default taskSchema;
