import User from '../models/User';

const userSchema = {
    name: 'User',
    target: User,
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true,
        },
        username: {
            type: "varchar",
            unique: true,
        },
        passwordHash: {
            type: "varchar",
        },
        addedOn: {
            type: 'timestamp',
        },
    },
    relations: {
        roles: {
            target: "Role",
            type: "many-to-many",
            joinTable: {
                name: 'user_role'
            }
        },
    },
};

export default userSchema;
