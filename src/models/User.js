class User {
    constructor (id, username, passwordHash, roles = []) {
        this.id = id;
        this.username = username;
        this.passwordHash = passwordHash;
        this.roles = roles;
    }
}

export default User;
