## Imports:
```javascript
import validate from '../validation/validate';
```

## Insert method:
```javascript
export const insert = async (req, { roleRepository }) => {
    try {
        const role = await validate('role', req.body);
        
        await roleRepository.save(role);
        return {
            status: 201,
            body: {
                message: `Role: ${role.name} saved.`,
            },
        };
    } catch (error) {
        return {
            status: 400,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

## Update method:
```javascript
export const update = async (req, { roleRepository }) => {
    try {
        const newRole = await validate('role', req.body);
        const role = await roleRepository.findOne({ id: req.params.id });
        if (!role) {
            return {
                status: 404,
                body: {
                    error: {
                        message: `Role with id: ${req.params.id} does not exists.`,
                    },
                },
            };
        }

        const existingRole = await roleRepository.findOne({ name: newRole.name });
        if (existingRole && existingRole.id !== parseInt(req.params.id)) {
            return {
                status: 400,
                body: {
                    message: `Role with name: ${newRole.name} already exists.`,
                },
            };
        }

        role.name = newRole.name;
        await roleRepository.save(role);

        return {
            status: 201,
            body: {
                message: `Role with id: ${req.params.id} updated.`,
            },
        };
        
    } catch (error) {
        return {
            status: 500,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

## Find role by name method:
```javascript
export const findOneByName = async (req, { roleRepository }) => {
    try {
        const role = await roleRepository.findOne({ name: req.params.name });
        if (!role) {
            return {
                status: 404,
                body: {
                    error: {
                        message: `Role ${req.params.name} does not exists.`,
                    },
                },
            };
        }

        return {
            status: 200,
            body: {
                role,
            },
        };
    } catch (error) {
        return {
            status: 500,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

## Find role by id method:
```javascript
export const findOneById = async (req, { roleRepository }) => {
    try {
        const role = await roleRepository.findOne({ id: req.params.id });
        if (!role) {
            return {
                status: 404,
                body: {
                    error: {
                        message: `Role with id: ${req.params.id} does not exists.`,
                    },
                },
            };
        }

        return {
            status: 200,
            body: {
                role,
            },
        };
    } catch (error) {
        return {
            status: 500,
            error: {
                message: error.message,
            },
        };
    }
};
```

## Find all roles method:
```javascript
export const findAll = async (req, { roleRepository }) => {
    const roles = await roleRepository.find();
    return {
        status: 200,
        body: {
            roles,
        },
    };
};
```

## Remove role method:
```javascript
export const remove = async (req, { roleRepository }) => {
    const role = await roleRepository.findOne({ id: req.params.id });

    if (!role) {
        return {
            status: 404,
            body: {
                error: {
                    message: `Role with id: ${req.params.id} does not exists.`,
                },
            },
        };
    }

    await roleRepository.removeById(req.params.id);

    return {
        status: 201,
        body: {
            message: `Role: "${role.name}" removed.`,
        },
    };
};
```
