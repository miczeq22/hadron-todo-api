## Imports:
```javascript
import validate from '../validation/validate';
```

## Find task by id method:
```javascript
export const findOneById = async (req, { taskRepository }) => {
    try {
        const task = await taskRepository.findOne({ id: req.params.id });

        if (!task) {
            return {
                status: 404,
                body: {
                    error: {
                        message: `Task with id: ${req.params.id} does not exists.`,
                    },
                },
            };
        }

        return {
            status: 200,
            body: {
                task,
            },
        };
    } catch (error) {
        return {
            status: 500,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

## Find all tasks for user method:
```javascript
export const findAllByUserId = async (req, { taskRepository, userRepository }) => {
    try {
        const user = await userRepository.findOne({ id: req.params.id });

        if (!user) {
            return {
                status: 404,
                body: {
                    error: {
                        message: `User with id: ${req.params.id} does not exists.`,
                    },
                },
            };
        }

        const tasks = await taskRepository.find({ userId: user.id });

        return {
            status: 200,
            body: {
                tasks,
            },
        };
    } catch (error) {
        return {
            status: 500,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

## Insert task method:
```javascript
export const insert = async (req, { taskRepository, userRepository }) => {
    try {
        const task = await validate('insertTask', req.body);
        const user = await userRepository.findOne({ id: task.userId });
        if (!user) {
            return {
                status: 404,
                body: {
                    error: {
                        message: `User with id: ${task.userId} does not exists.`,
                    },
                },
            };
        }

        await taskRepository.save(task);

        return {
            status: 201,
            body: {
                message: `Task "${task.title}" saved.`,
            },
        };
    } catch (error) {
        console.log(error);
        return {
            status: 500,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

## Update task method:
```javascript
export const update = async (req, { taskRepository, userRepository }) => {
    try {
        const newTask = await validate('insertTask', req.body);
        const task = await taskRepository.findOne({ id: req.params.id });
        if (!task) {
            return {
                status: 404,
                body: {
                    error: {
                        message: `Task with id: ${req.params.id} does not exists.`,
                    },
                },
            };
        }

        const user = await userRepository.findOne({ id: newTask.userId });
        if (!user) {
            return {
                status: 404,
                body: {
                    error: {
                        message: `User with id: ${newTask.userId} does not exists.`,
                    },
                },
            };
        }

        task.title = newTask.title;
        task.description = newTask.description;
        task.userId = newTask.userId;

        await taskRepository.save(task);

        return {
            status: 201,
            body: {
                message: `Task with id: ${req.params.id} updated.`,
            },
        }
        
    } catch (error) {
        return {
            status: 500,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

## Remove task method:
```javascript
export const remove = async (req, { taskRepository }) => {
    try {
        const task = await taskRepository.findOne({ id: req.params.id });
        if (!task) {
            return {
                status: 404,
                body: {
                    error: {
                        message: `Task with id: ${req.params.id} does not exists.`,
                    },
                },
            };
        }

        await taskRepository.removeById(req.params.id);

        return {
            status: 201,
            body: {
                message: `Task with id: ${req.params.id} removed.`,
            },
        };
    } catch (error) {
        return {
            status: 500,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```