## Imports: 
```javascript
import validate from '../validation/validate';
import { bcrypt } from '@brainhubeu/hadron-security';
import jwt from 'jsonwebtoken';
```
## Insert user method:
```javascript
export const insert = async (req, { userRepository }) => {
    try {
        const user = await validate('insertUser', req.body);

        const existingUser = await userRepository.findOne({ username: user.username });

        if (existingUser) {
            return {
                status: 400,
                body: {
                    error: {
                        message: `User: "${user.username}" already exists.`,
                    },
                },
            };
        }

        user.passwordHash = await bcrypt.hash(user.password);

        await userRepository.save(user);
    
        return {
            status: 201,
            body: {
                message: `User: "${user.username}" saved.`,
            },
        };
    } catch (error) {
        return {
            status: 400,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

## Login method:
```javascript
export const login = async (req, { userRepository }) => {
    try {
        const user = await userRepository.findOne({ username: req.body.username });
        if (!user) {
            return {
                status: 403,
                body: {
                    error: {
                        message: 'Unauthenticated',
                    },
                },
            };
        }

        const validPassword = await bcrypt.compare(req.body.password, user.passwordHash);
        
        if (!validPassword) {
            return {
                status: 403,
                body: {
                    error: {
                        message: 'Unauthenticated',
                    },
                },
            };
        }

        const secret = process.env.JWT_TOKEN || 'H4DR0N_S3CUR17Y';

        const token = jwt.sign({
            id: user.id,
            username: user.username,
        }, secret, {
            expiresIn: '2h'
        });

        return {
            status: 200,
            body: {
                token,
            },
        };

    } catch (error) {
        return {
            status: 500,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

## Update user method:
```javascript
export const update = async (req, { userRepository }) => {
    try {
        await validate('updateUser', req.body);

        const user = await userRepository.findOne({ id: req.params.id });

        if (!user) {
            return {
                status: 404,
                body: {
                    error: {
                        message: `User with id: "${req.params.id}" does not exist`,
                    },
                },
            };
        }

        const validPassword = await bcrypt.compare(req.body.oldPassword, user.passwordHash);

        if (!validPassword) {
            return {
                status: 403,
                body: {
                    error: {
                        message: 'Unauthenticated',
                    },
                },
            };
        }

        user.passwordHash = await bcrypt.hash(req.body.newPassword);

        await userRepository.save(user);

        return {
            status: 201,
            body: {
                message: `User: "${user.username}" updated.`,
            },
        };
    } catch (error) {
        return {
            status: 400,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

## Find user by username method:
```javascript
export const findOneByUsername = async (req, { userRepository }) => {
    try {
        const user = await userRepository.findOne({ username: req.params.username, relations: ['roles'] });

        if (!user) {
            return {
                status: 404,
                body: {
                    error: {
                        message: `User: ${req.params.username} does not exists.`,
                    },
                },
            };
        }

        return {
            status: 200,
            body: {
                user,
            },
        };

    } catch (error) {
        return {
            status: 400,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

## Find user by id method:
```javascript
export const findOneById = async (req, { userRepository }) => {
    try {
        const user = await userRepository.findOne({ id: req.params.id, relations: ['roles'] });

        if (!user) {
            return {
                status: 404,
                body: {
                    error: {
                        message: `User with id: ${req.params.id} does not exists.`,
                    },
                },
            };
        }

        return {
            status: 200,
            body: {
                user,
            },
        };

    } catch (error) {
        return {
            status: 400,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

## Find all users method:
```javascript
export const findAll = async (req, { userRepository }) => {
    try {
        const users = await userRepository.find({ relations: ['roles'] });

        return {
            status: 200,
            body: {
                users,
            },
        };

    } catch (error) {
        return {
            status: 500,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

## Remove user method:
```javascript
export const remove = async (req, { userRepository }) => {
    try {
        const user = await userRepository.findOne({ id: req.params.id });

        if (!user) {
            return {
                status: 404,
                body: {
                    error: {
                        message: `User with id: ${req.params.id} does not exists.`,
                    },
                },
            };
        }

        await userRepository.removeById(req.params.id);

        return {
            status: 201,
            body: {
                message: `User: "${user.username}" removed.`
            },
        };

    } catch (error) {
        return {
            status: 400,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```