# Todo-App with Hadron framework

### 1) Hadron-core installation and server init:
```bash
npm install --save @brainhubeu/hadron-core
npm install --save @brainhubeu/hadron-express
```

Simple hello world app

```javascript
// src/index.js
import hadron from '@brainhubeu/hadron-core';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

const port = process.env.PORT || 4000;
const expressApp = express();

expressApp.use(cors());
expressApp.use(bodyParser.json());

const config = {
    routes: {
        helloWorld: {
            path: '/',
            methods: ['GET'],
            callback: () => 'Hello World!',
        },
    },
};

const hadronInit = async () => {
    await hadron(expressApp, 
        [require('@brainhubeu/hadron-express')],
        config);
};

hadronInit();

expressApp.listen(port, () => {
    console.log(`Listening on http://localhost:${port}`);
});
```

### 2) Database (MySQL) and hadron-typeorm init:
Database with `Docker` installation:

Requirements: 
* Docker installed

MySQL installation:
```bash
docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d -p 3306:3306 mysql
```
Creating database:

---
#### Database configuration

    host: 127.0.0.1
    username: root
    password: my-secret-pw
---

Created database with name `todo-app`

hadron-typeorm and mysql installation:
```bash
npm install @brainhubeu/hadron-typeorm --save
npm install mysql --save
```

Database configuration for typeorm:
```javascript
const connection = {
  name: 'mysql-connection',
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'my-secret-pw',
  database: 'todo-app',
  entitySchemas: [],
  synchronize: true,
};

export default {
    connection,
    entities: []
};
```

Added hadron-typeorm to hadron core app in **src/index.js**:
```javascript
// src/index.js
const config = {
    ...db,
    routes: {
        helloWorld: {
            path: '/',
            methods: ['GET'],
            callback: () => 'Hello World!',
        },
    },
};

const hadronInit = async () => {
    await hadron(expressApp, [
            require('@brainhubeu/hadron-express'),
            require('@brainhubeu/hadron-typeorm'),
        ],
        config);
};
```

Added hadron-json-provider for routes init from specific path:
```bash
npm install --save @brainhubeu/hadron-json-provider
```

Now **src/index.js** is:
```javascript
import hadron from '@brainhubeu/hadron-core';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import db from './config/db';
import jsonProvider from '@brainhubeu/hadron-json-provider';

const port = process.env.PORT || 4000;
const expressApp = express();

expressApp.use(cors());
expressApp.use(bodyParser.json());

const hadronInit = async () => {

    const config = {
        ...db,
        routes: await jsonProvider([`${__dirname}/routes/*`], ['js']),
    };

    await hadron(expressApp, [
            require('@brainhubeu/hadron-express'),
            require('@brainhubeu/hadron-typeorm'),
        ],
        config);
};

hadronInit();

expressApp.listen(port, () => {
    console.log(`Listening on http://localhost:${port}`);
});
```

Added first entities/models - User and Role
##### Models:
```javascript
// src/models/User.js
class User {
    constructor (id, username, passwordHash, roles = []) {
        this.id = id;
        this.username = username;
        this.passwordHash = passwordHash;
        this.roles = roles;
    }
}

export default User;
// src/models/Role.js
class Role {
    constructor(id = 0, name) {
        this.id = id;
        this.name = name;
    }
}

export default Role;
```

##### Schemas: 
```javascript
// src/schemas/Role.js
import Role from '../models/Role';

const roleSchema = {
    name: 'Role',
    target: Role,
    columns: {
        id: {
            primary: true,
            type: 'int',
            generated: true,
        },
        name: {
            type: 'varchar',
        },
        addedOn: {
            type: 'timestamp',
        },
    },
};

export default roleSchema;

// src/schemas/User.js
import User from '../models/User';

const userSchema = {
    name: 'User',
    target: User,
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true,
        },
        username: {
            type: "varchar",
        },
        passwordHash: {
            type: "varchar",
        },
        addedOn: {
            type: 'timestamp',
        },
    },
    relations: {
        roles: {
            target: "Role",
            type: "many-to-many",
            joinTable: {
                name: 'user_role'
            }
        },
    },
};

export default userSchema;
```

Added schemas to database config:
```javascript
// src/config/db.js
import userSchema from '../schemas/User';
import roleSchema from '../schemas/Role';

const connection = {
  name: 'mysql-connection',
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'my-secret-pw',
  database: 'todo-app',
  entitySchemas: [roleSchema, userSchema],
  synchronize: true,
};

export default {
    connection,
    entities: [roleSchema, userSchema]
};
```

### 3) CRUD and validation for Role model:
hadron-validation installation:
```bash
npm install --save @brainhubeu/hadron-validation
```

Role validation schema:
```json
// src/validation/Role.json
{
    "type": "object",
    "properties": {
        "name": {
            "type": "string"
        }
    },
    "required": ["name"],
    "additionalProperties": false
}
```
schemas export:
```javascript
// src/validation/schemas.js
import roleSchema from './Role.json';

const schemas = {
    role: roleSchema,
};

export default schemas;
```
Validate function:
```javascript
import validatorFactory from '@brainhubeu/hadron-validation';
import schemas from './schemas';

export default validatorFactory(schemas);
```

Example findAll method in roleService: 
```javascript
export const findAll = async (req, { roleRepository }) => {
    const roles = await roleRepository.find();
    return {
        status: 200,
        body: {
            roles,
        },
    };
};
```
**Full service for `Role` can be found [here](./ROLE_CRUD.md)**

Created routes for `Role`:
```javascript
// src/routes/role.js
import * as service from '../services/roleService';

const routesConfig = () => {
    return {
        insertRole: {
            path: '/api/role',
            methods: ['POST'],
            callback: service.insert,
        },
        updateRole: {
            path: '/api/role/:id',
            methods: ['PUT'],
            callback: service.update,
        },
        findOneRoleByName: {
            path: '/api/role/byName/:name',
            methods: ['GET'],
            callback: service.findOneByName,
        },
        findOneRoleById: {
            path: '/api/role/:id',
            methods: ['GET'],
            callback: service.findOneById,
        },
        findAllRoles: {
            path: '/api/role',
            methods: ['GET'],
            callback: service.findAll,
        },
        removeRole: {
            path: '/api/role/:id',
            methods: ['DELETE'],
            callback: service.remove,
        },
    };
};

module.exports = routesConfig;
```
### 4) CRUD and validation for User model:

insert user validation schema
```json
// src/validation/insertUser.json
{
    "type": "object",
    "properties": {
        "username": {
            "type": "string"
        },
        "password": {
            "type": "string",
            "minLength": 8
        }
    },
    "required": ["username", "password"],
    "additionalProperties": false
}
```

Example insert method in userService:
```javascript
export const insert = async (req, { userRepository }) => {
    try {
        const user = await validate('insertUser', req.body);

        const existingUser = await userRepository.findOne({ username: user.username });

        if (existingUser) {
            return {
                status: 400,
                body: {
                    error: {
                        message: `User: "${user.username}" already exists.`,
                    },
                },
            };
        }

        user.passwordHash = await bcrypt.hash(user.password);

        await userRepository.save(user);
    
        return {
            status: 201,
            body: {
                message: `User: "${user.username}" saved.`,
            },
        };
    } catch (error) {
        return {
            status: 400,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

Full service for `User` can be found [here](./USER_CRUD.md)

Routes:
```javascript
// src/routes/user.js
import * as service from '../services/userService';

const routesConfig = () => {
    return {
        insertUser: {
            path: '/api/register',
            methods: ['POST'],
            callback: service.insert,
        },
        updateUser: {
            path: '/api/user/:id',
            methods: ['PUT'],
            callback: service.update,
        },
        findOneUserByUsername: {
            path: '/api/user/byUsername/:username',
            methods: ['GET'],
            callback: service.findOneByUsername,
        },
        findOneUserById: {
            path: '/api/user/:id',
            methods: ['GET'],
            callback: service.findOneById,
        },
        findAllUsers: {
            path: '/api/user',
            methods: ['GET'],
            callback: service.findAll,
        },
        removeUser: {
            path: '/api/user/:id',
            methods: ['DELETE'],
            callback: service.remove,
        },
    };
};

module.exports = routesConfig;
```

### 5) Basic security implementation:
```bash
npm i --save @brainhubeu/hadron-security
```

User and Role providers implementation:
**User Provider**:
```javascript
// src/security/UserProvider.js
class UserProvider {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }

    async loadUserByUsername(username) {
        const user = await this.userRepository.findOne({where: { username }, relations: ['roles']});
        return user;
    };

    refreshUser(user) {};
}

export default UserProvider;
```

**Role Provider**:
```javascript
// src/security/RoleProvider.js
class RoleProvider {
    constructor (roleRepository) {
        this.roleRepository = roleRepository;
    }

    async getRole(name) {
        return await this.roleRepository.findOne({ name });
    };

    async getRoles() {
        const roles = await this.roleRepository.find();
        return roles.map(role => role.name);
    };
}

export default RoleProvider;
```

**Security Configuration**:
```javascript
// src/security/securityConfig.js
import HadronSecurity from '@brainhubeu/hadron-security';

const securityConfig = (userProvider, roleProvider) => {
    return new Promise((resolve, reject) => {
        const security = new HadronSecurity(userProvider, roleProvider);

        security.allow('/api/user/*', ['Admin', 'NotExists']);

        resolve(security);
    });
};

export default securityConfig;
```

Added security express provider and token generation route in hadronInit:
```javascript
// src/index.js
const hadronInit = async () => {
    const config = {
        ...db,
        routes: await jsonProvider([`${__dirname}/routes/*`], ['js']),
    };


    const container = await hadron(expressApp, [
            require('@brainhubeu/hadron-typeorm'),
        ],
        config);

    const userProvider = new UserProvider(container.take('userRepository'));
    const roleProvider = new RoleProvider(container.take('roleRepository'));

    const security = await securityConfig(userProvider, roleProvider);

    expressApp.post('/login', generateTokenMiddleware(security));
    expressApp.use(expressMiddlewareProvider(security));

    hadronExpress.register(container, config);
    
    expressApp.use((req, res, next) => {
        res.status(404).json({
            error: {
                message: 'Not found.',
            },
        });
    });
};
```

### 6) Crud and validation for Task model:
Model:
```javascript
// src/models/Task.js
class Task {
    constructor (id, title, description, userId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.userId = userId;
    }
}

export default Task;
```
Schema:
```javascript
// src/schemas/Task.js
import Task from '../models/Task';

const taskSchema = {
    name: 'Task',
    target: Task,
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true,
        },
        title: {
            type: "varchar",
        },
        description: {
            type: "varchar",
        },
        userId: {
            type: 'int',
            required: true,
        },
        addedOn: {
            type: 'timestamp',
        },
    },
};

export default taskSchema;
```

Insert task schema:
```json
// src/validation/insertTask.json
{
    "type": "object",
    "properties": {
        "title": {
            "type": "string"
        },
        "description": {
            "type": "string",
            "minLength": 8
        },
        "userId": {
            "type": "number"
        }
    },
    "required": ["title", "description", "userId"],
    "additionalProperties": false
}
```

Example find all task for user method from service:
```javascript
export const findAllByUserId = async (req, { taskRepository, userRepository }) => {
    try {
        const user = await userRepository.findOne({ id: req.params.id });

        if (!user) {
            return {
                status: 404,
                body: {
                    error: {
                        message: `User with id: ${req.params.id} does not exists.`,
                    },
                },
            };
        }

        const tasks = await taskRepository.find({ userId: user.id });

        return {
            status: 200,
            body: {
                tasks,
            },
        };
    } catch (error) {
        return {
            status: 500,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```

**Full service for `Task` can be found [here](./TASK_CRUD.md)**

Routes:
```javascript
import * as service from '../services/taskService';

const routesConfig = () => {
    return {
        insertTask: {
            path: '/api/task',
            methods: ['POST'],
            callback: service.insert,
        },
        updateTask: {
            path: '/api/task/:id',
            methods: ['PUT'],
            callback: service.update,
        },
        findOneTaskById: {
            path: '/api/task/:id',
            methods: ['GET'],
            callback: service.findOneById,
        },
        findAllTasksByUser: {
            path: '/api/task/byUser/:id',
            methods: ['GET'],
            callback: service.findAllByUserId,
        },
        removeTask: {
            path: '/api/task/:id',
            methods: ['DELETE'],
            callback: service.remove,
        },
    };
};

module.exports = routesConfig;
```

### 7)Move login to global routes and secure tasks:

```javascript
// src/routes/global.js
import { login } from '../services/userService';

const routesConfig = () => {
    return {
        helloWorld: {
            path: '/',
            methods: ['GET'],
            callback: () => 'Hello World!',
        },
        version: {
            path: '/version',
            methods: ['GET'],
            callback: () => `App version: ${process.env.VERSION || '0.0.1'}`,
        },
        login: {
            path: '/api/login',
            methods: ['POST'],
            callback: login,
        },
    };
};

module.exports = routesConfig;
```
Login method in User Service: 
```javascript
// src/services/userService.js
export const login = async (req, { userRepository }) => {
    try {
        const user = await userRepository.findOne({ username: req.body.username });
        if (!user) {
            return {
                status: 403,
                body: {
                    error: {
                        message: 'Unauthenticated',
                    },
                },
            };
        }

        const validPassword = await bcrypt.compare(req.body.password, user.passwordHash);
        
        if (!validPassword) {
            return {
                status: 403,
                body: {
                    error: {
                        message: 'Unauthenticated',
                    },
                },
            };
        }

        const secret = process.env.JWT_TOKEN || 'H4DR0N_S3CUR17Y';

        const token = jwt.sign({
            id: user.id,
            username: user.username,
        }, secret, {
            expiresIn: '2h'
        });

        return {
            status: 200,
            body: {
                token,
            },
        };

    } catch (error) {
        return {
            status: 500,
            body: {
                error: {
                    message: error.message,
                },
            },
        };
    }
};
```
Secure task routes (Admin can do full CRUD, User can only read):
```javascript
// src/security/securityConfig.js
import HadronSecurity from '@brainhubeu/hadron-security';

const securityConfig = (userProvider, roleProvider) => {
    return new Promise((resolve, reject) => {
        const security = new HadronSecurity(userProvider, roleProvider);

        security
            .allow('/api/task/**', ['Admin', 'NotExists'], ['post', 'put', 'delete', 'get'])
            .allow('/api/task/**', 'User', ['get']);

        resolve(security);
    });
};

export default securityConfig;
```